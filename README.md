# 7th Summer School on AI @ IIITH

This Summer School Series is a research training event with a global scope aiming at equipping participants with the relevant advances in the critical and fast developing area of artificial intelligence and covers a wide range of topics on on AI with focus on Computer Vision & Machine Learning from a variety of perspectives where renowned academics and industry pioneers will lecture and share their views with the partiipants. 

---

A repository to store all the Compilation of material covered and resources distributed during the `7th summer school on AI with focus on Computer Vision and Machine Learning`

## Table of contents

- [7th Summer School on AI @ IIITH](#7th-summer-school-on-ai--iiith)
    - [Table of contents](#table-of-contents)
    - [Contents](#contents)
        - [Hands-on Sessions](#hands-on-sessions )
    

## Contents

### Hands-on Sessions
